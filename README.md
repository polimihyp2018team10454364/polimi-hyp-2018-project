## General information about the team and the web application

* Heroku URL: https://polimi-hyp-2018-team-10454364.herokuapp.com/

* Bitbucket repo URL: https://bitbucket.org/polimihyp2018team10454364/polimi-hyp-2018-project/src/master/

* Team Administrator: Giuseppe Mauri, 10454364, Giuseppe Mauri

* Team Member n.2: Giulia Meneghin, 10488640, Giulia Meneghin

## Description of the REST API

* /services?identifier=" + array[i] (identifier = id of the services in the database, array = is an array containing in each cell a service id where the staff member is responsible for)

* /services?identifier=" + array[i] (identifier = id of the services in the database, array = is an array containing in each cell a service id where the staff member helps in)

* /services?identifier=" + array[i] (identifier = id of the services in the database, array = is an array containing in each cell a service id of the services of the location)

* /staff?identifier=" + array[i] (identifier = id of the staff member in the database, array = is an array containing in each cell a staff id of who is responsable of this service)

* /staff?identifier=" + array[i] (identifier = id of the staff member in the database, array = is an array containing in each cell a staff id of who is a staff member this service)

* /locations?identifier=" + array[i] (identifier = id of the location in the database, array = is an array containing in each cell a location id of where this service is)

* /staff?sort=${sortby} (sort= sort strategy for the staff, alphaordered by surname)

* /services?sort=${sortby} (sort= sort strategy for the services, alphaordered by name)

* /services" + window.location.search (It is located within the searchServices() Function that allows you to take the last part of the Uri and insert it in the rest bees so as to fetch only the page that respects that query . The search property contains the query string portion of the current url.)

* /staff" + window.location.search (It is located within the searchStaff() Function that allows you to take the last part of the Uri and insert it in the rest bees so as to fetch only the page that respects that query . The search property contains the query string portion of the current url.)

* /locations" + window.location.search (It is located within the searchLocations() Function that allows you to take the last part of the Uri and insert it in the rest bees so as to fetch only the page that respects that query . The search property contains the query string portion of the current url.)

* /news" + window.location.search (It is located within the searchNews() Function that allows you to take the last part of the Uri and insert it in the rest bees so as to fetch only the page that respects that query . The search property contains the query string portion of the current url.)

* /news (In this case has not been adopted any type of strategy to load the news from the database in fact are loaded as they are found)

* /faq (In this case has not been adopted any type of strategy to load the faqs from the database in fact are loaded as they are found)

* /locations (In this case has not been adopted any type of strategy to load the location from the database in fact are loaded as they are found)

* /services (In this case has not been adopted any type of strategy to load the faqs from the database in fact are loaded as they are found)

## Division of tasks: 

* Giuseppe Mauri: Front-end: all Locations/Services/staff html, single location/service/staff html, changes to the css of the graphical interface of the site, css and html of the header.
Back-end: all Locations/Services/staff js, single location/service/staff js. Dynamics dropdown menu in the script.js and google maps iframe.

* Giulia Meneghin: Front-end: homepage, about us, contact us, faq, news html, writing all the json files, css and html of the footer.
Back-end: contact us, faq, news js, dynamics footer link in the script.js. Email.js and script to send real email

## Client-side languages used:

*  Html, Css, Javascript, Jquery.

## Template used:

* https://themefisher.com/products/medic-medical-template/ (The free version).

## External vendors' scripts/plugins:

* Bootstrap v3.3.7 (http://getbootstrap.com)

* Jquery Fancybox (http://fancybox.net/)

* TimePicker (https://timepicker.co/)

* Slick (http://kenwheeler.github.io/slick/)

* FontAwesome (https://fontawesome.com/)

* Jquery-ui (https://jqueryui.com/)

* Jquery(https://jquery.com/)

* Animate(https://daneden.github.io/animate.css/)

## Main Problems:

* Perform dropdown, as there was a bootstrap bug that prevented the menu from remaining showed when there was another dropdown menu inside it; then there was also the problem that if I clicked on an item of the menu more internal that in the menu before remained "active".

* Every time we changed a data on the Json file, or we modified the index.js file, we had to delete the folder "node_modules" and the DB.sql file because NPM at the command "start" gave error.

* There was a problem with bootstrap in the way it handles columns, which means if there are columns of different heights on a row, bootstrap tends to put the first element of the next row to the right of the highest column in the previous row, thus leaving blank spaces on the left. We fixed this problem by adding a css property "auto-clear" (line 3925 of style.css)