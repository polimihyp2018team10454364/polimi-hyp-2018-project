const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const sqlDbFactory = require("knex");
const process = require("process");
const fs = require('fs');
const nodemailer = require('nodemailer');
const _ = require("lodash");
const email = JSON.parse(fs.readFileSync("./other/data/email.json"));

let sqlDb;

function initSqlDB() {
	/* Locally we should launch the app with TEST=true to use SQLlite:

       > TEST=true node ./index.js

    */

//    process.env.TEST=true;
	if (process.env.TEST) {
		sqlDb = sqlDbFactory({
			client: "sqlite3",
			debug: true,
			connection: {
				filename: "./database.sqlite"
			},
			useNullAsDefault: true
		});
	} else {
		sqlDb = sqlDbFactory({
			debug: true,
			client: "pg",
			connection: process.env.DATABASE_URL,
			ssl: true
		});
	}
}

// staff table in the database
function initStaffDb() {
	return sqlDb.schema.hasTable("staff").then(exists => {
		if (!exists) {
			sqlDb.schema
				.createTable("staff", table => {
				table.increments();
				table.string("identifier");
				table.string("name");
				table.string("surname");
				table.string("responsibleFor");
				table.string("worksFor");
				table.text("generalData");
			})
				.then(() => {
				return Promise.all(
					_.map(staffList, d => {
						delete d.id;
						return sqlDb("staff").insert(d);
					})
				)
					.then(
					console.log("staff table created"));
			});
		} else {
			return true;
		}
	});
}


// locations table in the database
function initLocationDb() {
	return sqlDb.schema.hasTable("locations").then(exists => {
		if (!exists) {
			sqlDb.schema
				.createTable("locations", table => {
				table.increments();
				table.string("identifier");
				table.string("name");
				table.string("city");
				table.text("description");
				table.string("address");
				table.string("services");
			})
				.then(() => {
				return Promise.all(
					_.map(locationsList, l => {
						delete l.id;
						return sqlDb("locations").insert(l);
					})
				)
					.then(
					console.log("locations table created"));
			});
		} else {
			return true;
		}
	});
}



// services table in the database 
function initServiceDb() {
	return sqlDb.schema.hasTable("services").then(exists => {
		if (!exists) {
			sqlDb.schema
				.createTable("services", table => {
				table.increments();
				table.string("identifier");
				table.string("name");
				table.text("introduction");
				table.text("description");
				table.string("responsible");
				table.string("staff");
				table.string("locations");
			})
				.then(() => {
				return Promise.all(
					_.map(servicesList, s => {
						delete s.id;
						return sqlDb("services").insert(s);
					})
				)
					.then(
					console.log("services table created"));
			});
		} else {
			return true;
		}
	});
}


// photo of gallery table in the database 
function initPhotoDb() {
	return sqlDb.schema.hasTable("photos").then(exists => {
		if (!exists) {
			sqlDb.schema
				.createTable("photos", table => {
				table.increments();
				table.string("idPhoto");
				table.text("title");
				table.text("description");
			})
				.then(() => {
				return Promise.all(
					_.map(photosList, p => {
						delete p.id;
						return sqlDb("photos").insert(p);
					})
				)
					.then(
					console.log("photos table created"));
			});
		} else {
			return true;
		}
	});
}

// start part in the whoWeAre page
function initWhoWeAreTable() {
  return sqlDb.schema.hasTable("whoweare").then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable("whoweare", table => {
          table.increments();
          table.string("idPhoto");
          table.text("mention");
          table.text("briefHistory");
          table.text("mission");
          table.text("resultsAchieved");
        })
        .then(() => {
          return Promise.all(
            _.map(whoweareJson, w => {
              return sqlDb("whoweare").insert(w);
            })
          )
          .then(
            console.log("whoweare table created"));
        });
    } else {
      return true;
    }
  });
}

// news in generalInfo page
function initNewsTable() {
  return sqlDb.schema.hasTable("news").then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable("news", table => {
          table.increments();
          table.string("identifier");
          table.text("title");
          table.text("date");
          table.text("introduction");
          table.text("description");
        })
        .then(() => {
          return Promise.all(
            _.map(newsList, n => {
              delete n.id;
              return sqlDb("news").insert(n);
            })
          )
          .then(
            console.log("news table created"));
        });
    } else {
      return true;
    }
  });
}

// faq in Faq Page
function initFaqTable() {
  return sqlDb.schema.hasTable("faq").then(exists => {
    if (!exists) {
      sqlDb.schema
        .createTable("faq", table => {
          table.increments();
          table.string("identifier");
          table.text("title");
          table.text("description");
        })
        .then(() => {
          return Promise.all(
            _.map(faqList, f => {
              delete f.id;
              return sqlDb("faq").insert(f);
            })
          )
          .then(
            console.log("faq table created"));
        });
    } else {
      return true;
    }
  });
}

let serverPort = process.env.PORT || 5000;

var smtpTransport = nodemailer.createTransport({
  service : "gmail",
  "secure" : false,
  "port": 25,
  host: "smtp.gmail.com",
  auth: {
    user: email.user,
    pass: email.password
  },
  tls:{
    rejectUnauthorized: false
  }
});


let staffList = require("./other/data/staffData.json");
let locationsList = require("./other/data/locationsData.json");
let servicesList = require("./other/data/servicesData.json");
let photosList = require("./other/data/photosData.json");
let whoweareJson = require("./other/data/whoWeAreData.json");
let newsList = require("./other/data/newsData.json");
let faqList = require("./other/data/faqData.json");

	app.use(express.static(__dirname + "/public"));

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));

// email sender with nodemailer
app.get('/send',function(req,res){
    var mailOptions={
      from : '"FairyWorld" <fairyWorldTo@gmail.com>',
      username : req.query.username,
      to : req.query.to,
      subject : req.query.subject,
      text : req.query.text
    }
    console.log(mailOptions);

    smtpTransport.sendMail(mailOptions, function(error, response){
     if(error){
        console.log(error);
        res.send("error");
     }
     else{
        console.log("Message sent: " + mailOptions.text);
        console.log("We hope you will contact us again " + mailOptions.username + " !");
        res.send("sent");
        }
  });
});


	// /* Register REST entry point for staff */
	app.get("/staff", function(req, res) {
		let start = parseInt(_.get(req, "query.start", 0));
		let limit = parseInt(_.get(req, "query.limit", staffList.length));
		// variable for making allPeople
		let id = _.get(req, "query.identifier", "none");
		// variable for selecting doctors by locations
		let respFor = _.get(req, "query.responsibleFor", "none");
		let workFor = _.get(req, "query.worksFor", "none");
		let sortby = _.get(req, "query.sort", "none");

		let myQuery = sqlDb("staff");

		// analyze sortby query
		if (sortby === "surname") {
			myQuery = myQuery.orderBy("surname", "asc");
		}

		//allPeople	
		if (id !== "none") {
			myQuery = myQuery.where({identifier: id});
	}

	//select worksFor	
	if (workFor !== "none") {
		myQuery = myQuery.where({worksFor: workFor});
	}
	//select responsableFor	
	if (respFor !== "none") {
		myQuery = myQuery.where({responsibleFor: respFor});
	}
	// return result to fetch	
	myQuery.limit(limit)
    .offset(start).then(result => {
		res.send(JSON.stringify(result));
	});
});


// Register REST entry point for locations
app.get("/locations", function(req, res) {
	let id = _.get(req, "query.identifier", "none");
	let myQuery = sqlDb("locations");

	//allLocation
	if (id !== "none"){
		myQuery = myQuery.where({identifier: id});
	}

	myQuery.then(result => {
		res.send(JSON.stringify(result));
	});
});


// Register REST entry point for whoWeAre
app.get("/whoweare", function(req, res) {
	let myQuery = sqlDb("whoweare");

	myQuery.then(result => {
		res.send(JSON.stringify(result));
	});
});

// Register REST entry point for photos
app.get("/photos", function(req, res) {
	let id = _.get(req, "query.idPhoto", "none");
	let myQuery = sqlDb("photos");

	//allPhotos
	if (id !== "none"){
		myQuery = myQuery.where({idPhoto: id});
	}

	myQuery.then(result => {
		res.send(JSON.stringify(result));
	});
});

// Register REST entry point for news
app.get("/news", function(req, res) {
	let id = _.get(req, "query.identifier", "none");
	let myQuery = sqlDb("news");

	//allNews
	if (id !== "none"){
		myQuery = myQuery.where({identifier: id});
	}

	myQuery.then(result => {
		res.send(JSON.stringify(result));
	});
});

// Register REST entry point for faq
app.get("/faq", function(req, res) {
	let id = _.get(req, "query.identifier", "none");
	let myQuery = sqlDb("faq");

	//allNews
	if (id !== "none"){
		myQuery = myQuery.where({identifier: id});
	}

	myQuery.then(result => {
		res.send(JSON.stringify(result));
	});
});

// Register REST entry point for services
app.get("/services", function(req, res) {
	let start = parseInt(_.get(req, "query.start", 0));
	let limit = parseInt(_.get(req, "query.limit", servicesList.length));
	let id = _.get(req, "query.identifier", "none");

	// variable for selecting services by locations
	let loc = _.get(req, "query.locations", "none");
	let sortby = _.get(req, "query.sort", "none");

	// myQuery is the services table
	let myQuery = sqlDb("services");

	// analyze sortby query
	if (sortby === "name") {
		myQuery = myQuery.orderBy("name", "asc");
	}

	//allServices
	if (id !== "none") {
		myQuery = myQuery.where({identifier: id});
	}

	// selecting locations
	if (loc !== "none") {
		myQuery = myQuery.where({locations: loc});
	}

	myQuery.limit(limit).offset(start).then(result => {
		res.send(JSON.stringify(result));
	});
});



	

app.use(function(req, res) {
	res.status(400);
	res.send({ error: "400", title: "404: File Not Found" });
});

app.set("port", serverPort);

initSqlDB();
initStaffDb();
initLocationDb();
initServiceDb();
initPhotoDb();
initWhoWeAreTable();
initNewsTable();
initFaqTable();

/* Start the server on port 5000 */
app.listen(serverPort, function() {
	console.log(`Your app is ready at port ${serverPort}`);
});
