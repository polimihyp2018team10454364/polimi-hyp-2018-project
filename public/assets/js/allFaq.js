/* eslint-env browser */
console.log("# Starting up the application");



function addFaq(faq) {
  console.log("Load faq");
  $("#addFaq").append(
    `
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="headingOne">
<h4 class="panel-title">
<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#${faq.identifier}" aria-expanded="false" aria-controls="collapseOne">
${faq.title}
</a>
</h4>
</div>
<div id="${faq.identifier}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
<div class="panel-body">
${faq.description} 
</div>
</div>
</div>

`
  );
}


function addFaqList() {
	fetch(`/faq`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addFaq);
	});
}