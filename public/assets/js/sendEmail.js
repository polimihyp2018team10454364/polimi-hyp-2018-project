/* eslint-env browser */
console.log("# Starting up the application");


/*this script will  be used for the input form (sending an email) */
    
	function sendEmail(){
			var username, to, subject, text;
			$('#send_email').click(function(){
				username = $("#recipient_name").val();
				to = $("#recipient_email").val();
				subject = $("#email_subject").val();
				text = $("#email_text").val();
				$("p").remove("#e-status");
				$("#email_status").append(`<p style="margin-top: -10px; margin-left: 20px" id="e-status"><br>Sending an email to ${to}...</p>`);
				$.get("/send",{
					username:username, 
					to:to, 
					subject:subject, 
					text:text
				},function(data){
        		if(data=="sent"){
        			$("#error_message").remove();
        			$("p").remove("#e-response");
            		$("#sent_message").append(`<p style="margin-top: 10px; margin-left: 35px " id="e-response">An email has been sent to ${to}. Please check inbox!</p>`);
            		$("#email_status").empty();
       			}
       			else{
       				$("p").remove("#e-error");
       				$("#error_message").append(`<p style="margin-top: 10px; margin-left: 35px " id="e-error">Error! Email cannot be sent to ${to}</p>`);
       			}
			});
		});
		}
	