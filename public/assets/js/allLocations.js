/* eslint-env browser */
console.log("# Starting up the application");

function addLocationRow(location) {
  console.log("Load location");
  $("#myLocationRows").append(
    `
<div class="col-lg-4 col-md-4 col-sm-6">
<div class="location text-center">
<img src="../assets/img/locations/${location.identifier}.jpg" class="img-responsive" alt="location">
<h6>${location.name}</h6> 
<a href="singleLocation.html?identifier=${location.identifier}"  class="btn btn-main">Visit</a>
</div>
</div>
`
  );
}

let start = 0;
let count = 10;
let sortby = "name"; 

function updateLocationsList() {
  fetch(`/locations?start=${start}&limit=${count}&sort=${sortby}`)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      //clearTable();
      data.map(addLocationRow);
    });
}

function updateLists(){
  updateLocationsList();
}
