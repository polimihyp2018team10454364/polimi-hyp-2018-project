/* eslint-env browser */
console.log("# Starting up the application");

function addStaffRow(staff) {
	console.log("Load staff");
	$("#myStaffRows").append(
		`
<div class="col-lg-3 col-md-3 col-sm-4">
<div class="staff text-center">
<img src="../assets/img/staff/${staff.identifier}.jpg" class="img-responsive" alt="doctor">
<h6>${staff.surname} ${staff.name}</h6> 
<a href="singlePerson.html?identifier=${staff.identifier}"  class="btn btn-main">read more</a>
</div>
</div>
`
	);
}

let sortby = "surname"; 

function updateStaffList() {
	fetch(`/staff?sort=${sortby}`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addStaffRow);
	});
}

function updateLists(){
	updateStaffList();
}
