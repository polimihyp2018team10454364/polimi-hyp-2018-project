/* eslint-env browser */
console.log("# Starting up the application");



function addNews(news) {
  console.log("Load news");
  $("#addNews").append(
    `
<div class="col-md-12">
<div class="news">
<div class="col-md-5">
<img src="../assets/img/news/${news.identifier}.jpg" alt="news" class="img-responsive" >
</div>
<div class="content-text col-md-7">
<h6>${news.title}</h6>
<span>${news.date}</span>
<p>${news.introduction}</p>
<a href="../pages/singleNews.html?identifier=${news.identifier}" class="btn btn-main">read more</a>
</div>
</div>
</div>

`
  );
}


function addNewsList() {
	fetch(`/news`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addNews);
	});
}

 

