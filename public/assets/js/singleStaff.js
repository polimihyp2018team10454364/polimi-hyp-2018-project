var helpsIn;
var respoFor;

function loadStaff(staff) {
  console.log("Load staff");
  helpsIn=staff.worksFor;
	respoFor=staff.responsibleFor;
	
	
	
  $('#staffTitle').append(
    `
      ${staff.surname}
    `)
	
$('#breadcrumbTitle').append(
    `
${staff.surname} ${staff.name}
    `)
	

  $("#staffTopic").append(
    `
<div class="row col-md-12">
					<div class="panel-separator">
						<h3>"${staff.surname} ${staff.name}"</h3>
					</div>
					<div class="col-md-12">
						<div class="topic">
							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
								<img src="../assets/img/staff/${staff.identifier}.jpg" alt="staff" class="img-responsive" >
							</div>
							<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 ">
								<div class="panel-text">
									<h6>services</h6>
									<div id="respOfHeader">
                   					</div>
									<div id="helpsInHeader">
                   					</div>
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="panel-text">
									<h6>description</h6>
									<p>${staff.generalData}</p>
								</div>
							</div>

						</div>
					</div>
				</div>
	`
  );
  searchRespFor();
  searchHelpIn();
}



function setRespForHeader(service){
  console.log("Load services");
  $("#respOfHeader").append(
    `
    <h4>Responsible For:
</h4>
<p>
<ul id="respOfContent" class="fa-ul">
</ul>
</p>
    `
  );
}

function setRespForContent(service){
  console.log("Load services");
  $("#respOfContent").append(
    `
    <li><i class="fa-li fa fa-briefcase"></i><a href="singleService.html?identifier=${service.identifier}">${service.name}</a></li>
    `
  );
}

function setHelpsInHeader(service){
  console.log("Load services");
  $("#helpsInHeader").append(
    `
    <h4>Helps in:
</h4>
<p>
<ul id="helpsInContent" class="fa-ul">
</ul>
</p>
    `
  );
}

function setHelpsInContent(service){
  console.log("Load services");
  $("#helpsInContent").append(
    `
    <li><i class="fa-li fa fa-briefcase"></i><a href="singleService.html?identifier=${service.identifier}">${service.name}</a></li>
    `
  );
}

function searchRespFor() {
	if(respoFor!=null){
  var array = respoFor.split("n");
  var arrayLength = array.length;
			  if(arrayLength!=0){
		  setRespForHeader();
	  }
  for (var i = 0; i < arrayLength; i++) {
      console.log(array[i]);

  fetch("/services?identifier=" + array[i])
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(setRespForContent);
    });
  }
		}
}


function searchHelpIn() {
		if(helpsIn!=null){
  var array = helpsIn.split("n");
  var arrayLength = array.length;
				  if(arrayLength!=0){
		  setHelpsInHeader();
	  }
  for (var i = 0; i < arrayLength; i++) {
      console.log(array[i]);

  fetch("/services?identifier=" + array[i])
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(setHelpsInContent);
    });
  }
		}
}


function searchStaff() {
  console.log("response");
  fetch("/staff" + window.location.search)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(loadStaff);
    });
}
