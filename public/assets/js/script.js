$(document).ready(function () {
	
//	$('.dropdown-submenu').on('click', 'li', function() {
//    $(this).parents.('.dropdown li.active').removeClass('active');
//    $(this).addClass('active');
//});
 $('.dropdown-submenu li').click(function() {
	 $(this).addClass('active');
	 $('.dropdown li.active').removeClass('active');
	});
	
	 $('#location-menu').click(function() {
	 $(this).addClass('active');
	 $('.dropdown-menu li.active').removeClass('active');
	});
	
	$('.dropdown li').click(function() {
		$('.dropdown-submenu li.active').removeClass('active');
	});
//
//        $('.nav-tabs').on('shown.bs.tab', 'a', function(e) {
//        console.log(e.relatedTarget);
//        if (e.relatedTarget) {
//            $(e.relatedTarget).removeClass('active');
//        }
//    });   
	
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').find('.show').removeClass("show");
  }
  var $subMenu = $(this).next(".dropdown-menu");
  $subMenu.toggleClass('show');
		
		

	$(document).click(function(){
  $(".dropdown-submenu .show").removeClass("show");
});
		
  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass("show");
  });
  return false;
});
    
    
    var navHeight = $('.navbar').outerHeight();
    $(window).scroll(function () {
        if ($(window).scrollTop() > navHeight) {
            $('.navbar').addClass('fixed-header');
            $('#content').css('padding-top', navHeight + 'px');
        } else {
            $('.navbar').removeClass('fixed-header');
            $('#content').css('padding-top', '0');
        }
    });
});
(function($) {	
	"use strict";	
	
	//1.Hide Loading Box (Preloader)
	function handlePreloader() {
		if($('.preloader').length){
			$('.preloader').delay(200).fadeOut(500);
		}
	}	
	
	//6.Scroll to a Specific Div
	if($('.scroll-to-target').length){
		$(".scroll-to-target").click('click', function() {
			var target = $(this).attr('data-target');
		   // animate
		   $('html, body').animate({
			   scrollTop: $(target).offset().top
			 }, 1000);
	
		});
	}

	

	//26.Date picker
	function datepicker () {
	    if ($('#datepicker').length) {
	        $('#datepicker').datepicker();
	    };
	}

	//27.Select menu 
	function selectDropdown() {
	    if ($(".selectmenu").length) {
	        $(".selectmenu").selectmenu();

	        var changeSelectMenu = function(event, item) {
	            $(this).trigger('change', item);
	        };
	        $(".selectmenu").selectmenu({ change: changeSelectMenu });
	    };
	}	

	// Hero Slider
	$('.hero-slider').slick({
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		infinite: true,
		speed: 300,
		dots: true,
		arrows: true,
		fade: true,
		responsive: [
			{
				breakpoint: 600,
				settings: {
					arrows: false
				}
			}
		]
	});
	// Item Slider
	$('.items-container').slick({
		infinite: true,
		arrows: true,
		autoplay: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
					arrows: false
				}
			},
			{
				breakpoint: 525,
				settings: {
					slidesToShow: 1,
					arrows: false
				}
			}
		]
	});
	// Testimonial Slider
	$('.testimonial-carousel').slick({
		infinite: true,
		arrows: false,
		autoplay: true,
		slidesToShow: 2,
		dots: true,
		slidesToScroll: 2,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 525,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

/* ========================When document is loaded, do===================== */
	$(window).on('load', function() {
		// add your functions
		(function ($) {
			handlePreloader();
			datepicker();
		})(jQuery);
	});


})(window.jQuery);

// links1 footer
function addLinks1(service) {
	console.log("Load links1");
	$("#addLinks1").append(
        `
<li>
<a href="singleService.html?identifier=${service.identifier}">
<i class="fa fa-angle-right" aria-hidden="true"></i>${service.name}</a>
</li>

`
	);
}

// menu links footer pt.1
function addMenuLinks1() {
	fetch(`/services?start=${0}&limit=${3}`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addLinks1);
	});
}
// links2 footer
function addLinks2(service) {
	console.log("Load links2");
	$("#addLinks2").append(
        `
<li>
<a href="singleService.html?identifier=${service.identifier}">
<i class="fa fa-angle-right" aria-hidden="true"></i>${service.name}</a>
</li>

`
	);
}

// menu links footer pt.2
function addMenuLinks2() {
	fetch(`/services?start=${3}&limit=${3}`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addLinks2);
	});
}

