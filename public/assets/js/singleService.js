var responsible;
var staff;
var locations;

function loadService(service) {
  console.log("Load service");
  responsible = service.responsible;
  staff = service.staff;
  locations = service.locations;
  $('#serviceTitle').append(
    `
      ${service.name}
    `)
	
$('#breadcrumbTitle').append(
    `
${service.name}
    `)
	

  $("#serviceTopic").append(
    `
		<div class="row col-md-12">
					<div class="panel-separator">
						<h3>${service.name}</h3>
					</div>
					<div class="col-md-12">
						<div class="topic">
                            <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
								<img src="../assets/img/services/${service.identifier}.jpg" alt="Service" class="img-responsive" >
							</div>
                            <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
								<div class="panel-text">
									<h6>Description</h6>
									<p>${service.description}</p>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="panel-text">
									<h6>Responsable</h6>
									<p>
									<ul id="responsible" class="fa-ul">
                   						</ul>
									</p>
									<h6>STAff</h6>
									<p>	
										<ul id="staffList" class="fa-ul">
                   						</ul>
									</p>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="panel-text">
									<h6>Where</h6>
									<p>
										<ul id="locationsList" class="fa-ul">
                   						</ul>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
	`
  );
  searchResponsible();
  searchStaff();
  searchLocations();
}


function setResponsible(responsible){
  console.log("Load responsible");
  $("#responsible").append(
    `
    <li><i class="fa-li fa fa-user-md"></i><a href="singlePerson.html?identifier=${responsible.identifier}">${responsible.surname} ${responsible.name}</a></li>
    `
  );
}

function setStaff(staff){
  console.log("Load staff");
  $("#staffList").append(
    `
    <li><i class="fa-li fa fa-user-md"></i><a href="singlePerson.html?identifier=${staff.identifier}">${staff.surname} ${staff.name}</a></li>
    `
  );
}

function setLocations(location){
  console.log("Load location");
  $("#locationsList").append(
    `
    <li><i class="fa-li fa fa-hospital-o"></i><a href="singleLocation.html?identifier=${location.identifier}">${location.name}</a></li>
    `
  );
}

function searchService() {
  console.log("response");
  fetch("/services" + window.location.search)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(loadService);
    });
}



function searchResponsible() {
	var array = responsible.split("n");
  var arrayLength = array.length;
  for (var i = 0; i < arrayLength; i++) {
  fetch("/staff?identifier=" + array[i])
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(setResponsible);
    });
  }
}

function searchStaff() {
  var array = staff.split("n");
  var arrayLength = array.length;
  for (var i = 0; i < arrayLength; i++) {
  fetch("/staff?identifier=" + array[i])
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(setStaff);
    });
  }
}

function searchLocations() {
  var array = locations.split("n");
  var arrayLength = array.length;
  for (var i = 0; i < arrayLength; i++) {
      console.log(array[i]);
  fetch("/locations?identifier=" + array[i])
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(setLocations);
    });
  }
}