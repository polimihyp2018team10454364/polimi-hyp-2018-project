function loadNews(news) {
  console.log("Load news");

  $('#newsTitle').append(
    `
      ${news.title}
    `)
	
$('#breadcrumbTitle').append(
    `
${news.title}
    `)
	

  $("#newsTopic").append(
    `
<div class="panel-separator col-md-12 col-sm-12">
<h3>${news.title}</h3><span>${news.date}</span>
</div>
<div class="col-md-12">
<div class="topic">
<div class=" container col-md-5">
<a href="faq.html"><img src="../assets/img/news/${news.identifier}.jpg" alt="" class="img-responsive"></a>
</div>
<div class="content-text col-md-7">
<div class="panel-text">
<p>${news.introduction}<br>
${news.description}</p>
</div>
</div>
</div>
</div>
	`
  );
  
}

function searchNews() {
  console.log("response");
  fetch("/news" + window.location.search)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(loadNews);
    });
}
