/* eslint-env browser */
console.log("# Starting up the application");

function addWhoWeAre(whoWeAre) {
  console.log("Load whoWeAre");
  $("#startPartWhoWeAre").append(
    `
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
<img src="../assets/img/about/${whoWeAre.idPhoto}.jpg" class="img-responsive" alt="story">
</div>
<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
<div class="panel-separator">
<h3>Our Story</h3>
</div>
<div class="story-content" >
<h6 class="tagline"style="text-align: center">"${whoWeAre.mention}"</h6>
<h6>
Brief history</h6>
<p>"${whoWeAre.briefHistory}"</p>
<h6>Mission</h6>
<p>"${whoWeAre.mission}"</p>
<h6>Results achieved</h6>
<p>"${whoWeAre.resultsAchieved}"</p>
</div>
</div>
`
  );
}

function addGallery(photo) {
  console.log("Load photo");
  $("#startPartGallery").append(
    `
<div class="col-md-3 col-xs-6">
<div class="gallery-item">
<img src="../assets/img/about/${photo.idPhoto}.jpg" class="img-responsive" alt="gallery-image">
<a data-fancybox="images" href="../assets/img/about/${photo.idPhoto}.jpg">
<h3>"${photo.title}"</h3>
<p>"${photo.description}"</p></a>
</div>
</div>
`
  );
}

function updateGallery() {
	fetch(`/photos`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addGallery);
	});
}

 

function updateWhoWeAre() {
  fetch(`/whoweare`)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
	  data.map(addWhoWeAre);
    });
}

