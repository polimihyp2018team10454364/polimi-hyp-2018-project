var servicesList;

function loadLocation(location) {
  console.log("Load location");
  servicesList=location.services;
	
	
	
  $('#locationTitle').append(
    `
      ${location.name}
    `)
	
$('#breadcrumbTitle').append(
    `
${location.name}
    `)
	

  $("#locationTopic").append(
    `
<div class="row col-md-12">
					<div class="panel-separator">
						<h3>"${location.name}"</h3>
					</div>
					<div class="col-md-12">
						<div class="topic">
							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
								<img src="../assets/img/locations/${location.identifier}.jpg" alt="location" class="img-responsive" >
							</div>
							<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
								<div class="panel-text">
									<h6>offered services</h6>
									<p>
									<ul id="servicesList" class="fa-ul">
                   						</ul>
									</p>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="panel-text">
									<h6>Description</h6>
									<p>${location.description}</p>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="panel-text">
									<h6>visit us</h6>
									<div class="map">
								<iframe width="100%" height="330px"
										src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBtg9DjiSP2Jcq9CM2g1rXNVI_pj63nUWI&q=${location.address}" 
										frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>

	`
  );
  searchServices();
}



function setServices(service){
  console.log("Load services");
  $("#servicesList").append(
    `
    <li><i class="fa-li fa fa-briefcase"></i><a href="singleService.html?identifier=${service.identifier}">${service.name}</a></li>
    `
  );
}

function searchLocation() {
  console.log("response");
  fetch("/locations" + window.location.search)
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(loadLocation);
    });
}


function searchServices() {
  var array = servicesList.split("n");
  var arrayLength = array.length;
  for (var i = 0; i < arrayLength; i++) {
      console.log(array[i]);
  fetch("/services?identifier=" + array[i])
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      data.map(setServices);
    });
  }
}