/* eslint-env browser */
console.log("# Starting up the application");
var filtername = "None";
let start = 0;
let count = 10;
let sortby = "name";
let locId = "none";

function clearTable() {
	$("div").remove("#allLocation");
	$("div").remove("#tabContent");
}

function addAllLocationTabHeader() {
	console.log("Load Header");
	$("#myAllServiceHeader").append(
		`
<div class="service-groups tab-pane fade in active" id="allLocation">
<div class="panel-separator col-md-12 col-sm-12">
<h3>OUR SERVICES</h3>
</div>
<div class="auto-clear">
<div id="myServiceRows">
<!--Dinamically add Services to the page-->
</div>
</div>
</div>
`
	);
}

function addServiceRow(service) {
	console.log("Load service");
	$("#myServiceRows").append(
		`
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="service text-center">
<img src="../assets/img/services/${service.identifier}.jpg" class="img-responsive" alt="Service">
<h6>${service.name}</h6>
<p>${service.introduction}</p>
<a href="singleService.html?identifier=${service.identifier}" class="btn btn-main">Read More</a>
</div>
</div>
`
	);
}

function addFilteredTabHeader() {
	console.log("Load Header");
	$("#myFilteredTabHeader").append(
		`
<div id="tabContent" class="service-groups tab-pane fade in" id="${filtername}">
<div class="panel-separator col-md-12">
<h3>${filtername}</h3>
</div>
<div class="auto-clear">
<div id="myFilteredTabRows">
<!--Dinamically add Services to the page-->
</div>
</div>
</div>
`
	);
}


function addFilteredTabContent(service) {
	console.log("Load Tab");
	$("#myFilteredTabRows").append(
		`
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="service text-center">
<img src="../assets/img/services/${service.identifier}.jpg" class="img-responsive" alt="Service">
<h6>${service.name}</h6>
<p>${service.introduction}</p>
<a href="singleService.html?identifier=${service.identifier}" class="btn btn-main">read more</a>
</div>
</div>
`
	);
}


function showLocations(loc) {
	$("#location-menu").append(
		`
<li><a class="dropdown-item" onclick="filterLocation('${loc.identifier}','${loc.city}')" href="#${loc.city}" data-toggle="tab"> ${loc.city} </a></li>
`
	);
}

function addAllLocation(){
	clearTable()
	addAllLocationTabHeader();
	updateServicesList();
}

function filterLocation(x, y){
	clearTable()
	locId = x;
	filtername = y;
	addFilteredTabHeader();
	createFilteredList();

}

function searchLocations() {
	fetch(`/locations`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(showLocations);
	});

}

function updateServicesList() {
	fetch(`/services?sort=${sortby}`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(addServiceRow);
	});
}

function createFilteredList() {
	fetch(`/services`)
		.then(function(response) {
		return response.json();
	})
		.then(function(data) {
		data.map(filter);
	});
}

function filter(service) {
	var array = service.locations.split("n");
	var arrayLength = array.length;
	for (var i = 0; i < arrayLength; i++) {
		if(array[i] === locId)
		{
			addFilteredTabContent(service);
		}
	}
}